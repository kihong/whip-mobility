import { join } from 'path'

import { registerAs } from '@nestjs/config'
import * as admin from 'firebase-admin'

export default registerAs('fba', () => ({
  credential: admin.credential.cert(join(process.cwd(), 'firebase.json')),
  databaseURL: process.env.FIREBASE_DATABASE,
}))
