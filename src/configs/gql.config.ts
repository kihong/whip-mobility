import { registerAs } from '@nestjs/config'

export default registerAs('gql', () => ({
  endpoint: process.env.GQL_ENDPOINT,
}))
