import { registerAs } from '@nestjs/config'

export default registerAs('db', () => ({
  mongo: {
    host: process.env.MONGO_HOST,
    port: parseInt(process.env.MONGO_PORT, 10) || 27017,
    user: process.env.MONGO_USER,
    name: process.env.MONGO_NAME,
    password: process.env.MONGO_PASSWORD,
  },
}))
