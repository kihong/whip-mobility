import { registerAs } from '@nestjs/config'

export default registerAs('jwt', () => ({
  secret: process.env.JWT_SECRET,
  expiry: !isNaN(process.env.JWT_EXPIRY as any)
    ? parseInt(process.env.JWT_EXPIRY, 10)
    : process.env.JWT_EXPIRY,
}))
