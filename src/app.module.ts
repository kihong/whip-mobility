import { Module, ValidationPipe } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { APP_PIPE } from '@nestjs/core'
import { GraphQLModule } from '@nestjs/graphql'
import { MongooseModule } from '@nestjs/mongoose'
import { FirebaseAdminCoreModule } from '@tfarras/nestjs-firebase-admin'

import appConfig from './configs/app.config'
import dbConfig from './configs/db.config'
import fbaConfig from './configs/fba.config'
import gqlConfig from './configs/gql.config'
import jwtConfig from './configs/jwt.config'
import { AuthModule } from './lib/auth/auth.module'
import { DealerModule } from './lib/dealer/dealer.module'
import { ServiceModule } from './lib/service/service.module'
import { UserModule } from './lib/user/user.module'
import { VehicleModule } from './lib/vehicle/vehicle.module'
import { GraphqlSetting, MongooseSetting } from './settings'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.development', '.env.staging', '.env.production', '.env'],
      isGlobal: true,
      load: [appConfig, dbConfig, jwtConfig, fbaConfig, gqlConfig],
    }),
    MongooseModule.forRootAsync({
      useClass: MongooseSetting,
    }),
    GraphQLModule.forRootAsync({
      useClass: GraphqlSetting,
    }),
    FirebaseAdminCoreModule.forRootAsync({
      useFactory: (config: ConfigService) => config.get('fba'),
      inject: [ConfigService],
    }),
    UserModule,
    VehicleModule,
    ServiceModule,
    DealerModule,
    AuthModule,
  ],
  providers: [
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({ transform: true }),
    },
  ],
})
export class AppModule {}
