import { createParamDecorator } from '@nestjs/common'
import { Request, Response } from 'express'

export const Res = createParamDecorator((data, [_root, _args, ctx, _info]): Response => ctx.res)

export const Req = createParamDecorator((data, [_root, _args, ctx, _info]): Request => ctx.req)
