import { ExecutionContext, ForbiddenException, createParamDecorator } from '@nestjs/common'
import { GqlContextType, GqlExecutionContext } from '@nestjs/graphql'
import { FirebaseUser } from '@tfarras/nestjs-firebase-auth'

export const AppUser = createParamDecorator((data: unknown, context: ExecutionContext) => {
  let user: FirebaseUser

  if (context.getType<GqlContextType>() === 'graphql') {
    const ctx = GqlExecutionContext.create(context)
    user = ctx.getContext().req.user
  } else {
    const request = context.switchToHttp().getRequest()
    user = request.user
  }

  if (!user)
    throw new ForbiddenException(
      'Guard is missing!',
      'Can someone ask the developer if the guard is on leave?'
    )

  return typeof data === 'string' && data ? user && user[data] : user
})
