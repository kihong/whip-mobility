export class AppRegex {
  static plateNo = {
    MY: new RegExp('^[a-zA-Z][a-zA-Z0-9]+$'),
    US: new RegExp('^[a-zA-Z][a-zA-Z0-9]+$'),
  }
}
