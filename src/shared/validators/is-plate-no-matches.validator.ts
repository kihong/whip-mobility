import { BadRequestException } from '@nestjs/common'
import { RegisterVehicleArgs } from '~/lib/vehicle/vehicle.dto'
import {
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  registerDecorator,
} from 'class-validator'

import { AppRegex } from '../utils'

@ValidatorConstraint({ name: 'isPlateNoMatches', async: false })
class IsPlateNoMatchesConstraint implements ValidatorConstraintInterface {
  public validate(propertyValue: string, args: ValidationArguments) {
    const isoCode = (args.object as RegisterVehicleArgs).manufacture.country
    const regex: RegExp = AppRegex.plateNo[isoCode]
    if (!regex) {
      throw new BadRequestException(`This vehicle origin: "${isoCode}" is not compatible yet.`)
    }
    return regex.test(propertyValue)
  }

  public defaultMessage(args: ValidationArguments) {
    const isoCode = (args.object as RegisterVehicleArgs).manufacture.country
    return `${args.property} does not match ${isoCode} vehicle plate no pattern`
  }
}

export function IsPlateNoMatches(validationOptions?: ValidationOptions) {
  return (object: Record<string, any>, propertyName: string) =>
    registerDecorator({
      name: 'isPlateNoMatches',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: IsPlateNoMatchesConstraint,
    })
}
