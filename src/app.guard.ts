import { ExecutionContext, Injectable } from '@nestjs/common'
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host'
import { GqlContextType, GqlExecutionContext } from '@nestjs/graphql'
import { AuthGuard } from '@nestjs/passport'

import { PassportType } from './lib/auth/auth.interface'

@Injectable()
export class AppGuard extends AuthGuard([PassportType.FIREBASE]) {
  canActivate(context: ExecutionContext) {
    if (context.getType<GqlContextType>() === 'graphql') {
      const ctx = GqlExecutionContext.create(context)
      const { req } = ctx.getContext()
      return super.canActivate(new ExecutionContextHost([req]))
    }

    return super.canActivate(context)
  }

  getRequest(context: ExecutionContext) {
    if (context.getType<GqlContextType>() === 'graphql') {
      const ctx = GqlExecutionContext.create(context)
      return ctx.getContext().req
    }

    return context.switchToHttp().getRequest()
  }
}
