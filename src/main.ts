import { Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'

import { AppModule } from './app.module'

declare const module: any

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  const configService: ConfigService = app.get('ConfigService')
  const port = configService.get<number>('app.port')

  await app.listen(port, () => {
    Logger.log(`Server is now listening on port ${port}...`, 'NestFactory')
  })

  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }
}
bootstrap()
