import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { MongooseModuleOptions, MongooseOptionsFactory } from '@nestjs/mongoose'

@Injectable()
export class MongooseSetting implements MongooseOptionsFactory {
  constructor(private readonly configService: ConfigService) {}

  createMongooseOptions(): MongooseModuleOptions {
    const host = this.configService.get<string>('db.mongo.host')
    const port = this.configService.get<number>('db.mongo.port')
    const name = this.configService.get<string>('db.mongo.name')
    const user = this.configService.get<string>('db.mongo.user')
    const pass = this.configService.get<string>('db.mongo.password')
    const uri = `mongodb://${user}:${pass}@${host}:${port}/${name}?authSource=admin`

    return {
      uri,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }
  }
}
