import { join } from 'path'

import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { GqlModuleOptions, GqlOptionsFactory } from '@nestjs/graphql'

@Injectable()
export class GraphqlSetting implements GqlOptionsFactory {
  constructor(private readonly configService: ConfigService) {}
  createGqlOptions(): Promise<GqlModuleOptions> | GqlModuleOptions {
    const path = this.configService.get<string>('gql.endpoint', 'gql')

    return {
      context: ({ req, res, connection }) => {
        if (connection) {
          return {
            req: {
              ...req,
              ...connection.context,
              headers: {
                ...req.headers,
                ...connection.context.headers,
              },
            },
            res,
          }
        }
        return { req, res }
      },
      subscriptions: {
        keepAlive: 10,
      },
      autoSchemaFile: join(process.cwd(), 'src/shared/schemas/.generated.gql'),
      installSubscriptionHandlers: true,
      resolverValidationOptions: {
        requireResolversForResolveType: false,
      },
      debug: true,
      introspection: true,
      playground: true,
      cors: false,
      path,
    }
  }
}
