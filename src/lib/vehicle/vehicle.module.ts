import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { AuthService } from '../auth/auth.service'
import { VehicleController } from './vehicle.controller'
import { VehicleResolver } from './vehicle.resolver'
import { VehicleSchema } from './vehicle.schema'
import { VehicleService } from './vehicle.service'

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Vehicle', schema: VehicleSchema }])],
  controllers: [VehicleController],
  providers: [AuthService, VehicleResolver, VehicleService],
})
export class VehicleModule {}
