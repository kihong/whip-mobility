import { registerEnumType } from '@nestjs/graphql'
import { Document, Types } from 'mongoose'

export class VehicleDoc extends Document {
  brand: string
  type: VehicleType
  owner: string
  drivers: Types.Array<string>
  createdAt: Date
  updatedAt: Date
}

export enum VehicleType {
  HATCHBACK = 'HATCHBACK',
  SEDAN = 'SEDAN',
  SUV = 'SUV',
  CROSSOVER = 'CROSSOVER',
  COUPE = 'COUPE',
  CONVERTIBLE = 'CONVERTIBLE',
}

registerEnumType(VehicleType, {
  name: 'VehicleType',
})
