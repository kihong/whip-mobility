import { UseGuards } from '@nestjs/common'
import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql'
import { FirebaseUser } from '@tfarras/nestjs-firebase-auth'
import { AppGuard } from '~/app.guard'
import { AppUser } from '~/shared/decorators'

import { UserObject } from '../user/user.object'
import { RegisterVehicleArgs } from './vehicle.dto'
import { VehicleDoc } from './vehicle.interface'
import { VehicleObject } from './vehicle.object'
import { VehicleService } from './vehicle.service'

@Resolver(_of => VehicleObject)
@UseGuards(AppGuard)
export class VehicleResolver {
  constructor(private readonly vehicleService: VehicleService) {}

  @Query(_returns => [VehicleObject])
  async myVehicles(@AppUser() { uid }: FirebaseUser) {
    return await this.vehicleService.getVehicles(uid)
  }

  @Mutation(_returns => VehicleObject)
  async registerVehicle(@AppUser() { uid }: FirebaseUser, @Args() data: RegisterVehicleArgs) {
    return this.vehicleService.createVehicle(uid, data)
  }

  @ResolveField('id')
  id(@Parent() { id }: VehicleDoc) {
    return id.toString()
  }

  @ResolveField('owner', _returns => UserObject)
  owner(@Parent() { owner }: VehicleDoc) {
    return this.vehicleService.getOwner(owner)
  }

  @ResolveField('drivers', _returns => [UserObject])
  drivers(@Parent() { drivers }: VehicleDoc) {
    return this.vehicleService.getDrivers(drivers)
  }
}
