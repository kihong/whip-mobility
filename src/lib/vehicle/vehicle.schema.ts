import { countries } from 'country-code-lookup'
import { Schema } from 'mongoose'

import { VehicleType } from './vehicle.interface'

export const VehicleSchema = new Schema(
  {
    brand: String,
    plateNo: {
      type: String,
      unique: true,
      trim: true,
      uppercase: true,
    },
    manufacture: {
      country: {
        type: String,
        enum: countries.map(c => c.iso2),
        required: true,
      },
      year: Date,
    },
    type: {
      type: String,
      enum: Object.keys(VehicleType),
      required: true,
      default: VehicleType.SEDAN,
    },
    owner: {
      type: String,
      ref: 'User',
    },
    drivers: [
      {
        type: String,
        ref: 'User',
      },
    ],
  },
  { timestamps: true }
)
