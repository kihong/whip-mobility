import { ArgsType, Field, InputType } from '@nestjs/graphql'
import { IsPlateNoMatches } from '~/shared/validators'
import { Type } from 'class-transformer'
import {
  IsDate,
  IsEnum,
  IsISO31661Alpha2,
  IsNotEmpty,
  IsString,
  ValidateIf,
  ValidateNested,
} from 'class-validator'
import { countries } from 'country-code-lookup'

import { VehicleType } from './vehicle.interface'

@InputType()
class VehicleManufactureInput {
  @Field()
  @IsISO31661Alpha2()
  readonly country: string

  @Field({ nullable: true })
  @IsDate()
  readonly year?: Date
}

@ArgsType()
export class RegisterVehicleArgs {
  @Field()
  @IsString()
  @IsNotEmpty()
  readonly brand: string

  @Field(_ => VehicleType)
  @IsEnum(VehicleType, {
    message: `Type must be a valid enum value (${Object.keys(VehicleType).join()})`,
  })
  readonly type: VehicleType

  @Field()
  @ValidateIf((o: RegisterVehicleArgs) =>
    countries.map(c => c.iso2).includes(o.manufacture.country)
  )
  @IsPlateNoMatches()
  readonly plateNo: string

  @Field(_ => VehicleManufactureInput)
  @Type(_ => VehicleManufactureInput)
  @ValidateNested()
  readonly manufacture: VehicleManufactureInput
}
