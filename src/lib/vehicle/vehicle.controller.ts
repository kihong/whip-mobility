import { Controller, Get, UseGuards } from '@nestjs/common'
import { FirebaseUser } from '@tfarras/nestjs-firebase-auth'
import { AppGuard } from '~/app.guard'
import { AppUser } from '~/shared/decorators'

import { VehicleService } from './vehicle.service'

@Controller('vehicle')
@UseGuards(AppGuard)
export class VehicleController {
  constructor(private readonly vehicleService: VehicleService) {}
  @Get()
  async getVehicles(@AppUser() { uid }: FirebaseUser) {
    const vehicles = await this.vehicleService.getVehicles(uid)
    return vehicles
  }
}
