import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'

import { AuthService } from '../auth/auth.service'
import { RegisterVehicleArgs } from './vehicle.dto'
import { VehicleDoc } from './vehicle.interface'

@Injectable()
export class VehicleService {
  constructor(
    @InjectModel('Vehicle') private VehicleModel: Model<VehicleDoc>,
    private readonly authService: AuthService
  ) {}

  getOwner(userId: string) {
    return this.authService.getUser(userId)
  }

  async getDrivers(userIds: string[]) {
    return await Promise.all(userIds.map(uid => this.authService.getUser(uid)))
  }

  async getVehicles(uid: string) {
    const vs = await this.VehicleModel.find({
      $or: [{ owner: uid }, { drivers: uid }],
    })

    return vs
  }

  async createVehicle(userId: string, data: RegisterVehicleArgs) {
    const vehicle = new this.VehicleModel({
      ...data,
      owner: userId,
    })

    return await vehicle.save()
  }
}
