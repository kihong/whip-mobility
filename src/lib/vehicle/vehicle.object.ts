import { Field, ID, ObjectType } from '@nestjs/graphql'

import { UserObject } from '../user/user.object'
import { VehicleType } from './vehicle.interface'

@ObjectType()
export class VehicleManufactureObject {
  @Field()
  country: string

  @Field(_ => Date, { nullable: true })
  year?: Date
}

@ObjectType()
export class VehicleObject {
  @Field(_ => ID)
  id: string

  @Field()
  brand: string

  @Field(_ => VehicleType)
  type: VehicleType

  @Field(_ => UserObject)
  owner: UserObject

  @Field(_ => [UserObject])
  drivers: UserObject[]

  @Field(_ => VehicleManufactureObject)
  manufacture: VehicleManufactureObject

  @Field(_ => Date)
  createdAt: Date

  @Field(_ => Date)
  updatedAt: Date
}
