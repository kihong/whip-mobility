import { Schema } from 'mongoose'

export const DealerSchema = new Schema({
  name: String,
  hour: {
    open: Number,
    close: Number,
  },
  technicians: [
    {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
})
