import { Controller, Get, Param } from '@nestjs/common'

import { AuthService } from './auth.service'

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @Get('token/:uid')
  async testError(@Param('uid') uid: string) {
    return this.authService.getCustomToken(uid)
  }
}
