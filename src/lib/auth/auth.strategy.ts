import { Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { FirebaseAuthStrategy, FirebaseUser } from '@tfarras/nestjs-firebase-auth'
import { ExtractJwt } from 'passport-jwt'

import { PassportType } from './auth.interface'

@Injectable()
export class FirebaseStrategy extends PassportStrategy(
  FirebaseAuthStrategy,
  PassportType.FIREBASE
) {
  public constructor() {
    super({
      extractor: ExtractJwt.fromAuthHeaderAsBearerToken(),
    })
  }

  async validate(payload: FirebaseUser): Promise<FirebaseUser> {
    return payload
  }
}
