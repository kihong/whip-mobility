import { Module } from '@nestjs/common'
import { PassportModule } from '@nestjs/passport'

import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { FirebaseStrategy } from './auth.strategy'

@Module({
  imports: [PassportModule],
  providers: [FirebaseStrategy, AuthService],
  exports: [FirebaseStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
