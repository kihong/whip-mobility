import { Inject, Injectable } from '@nestjs/common'
import {
  FIREBASE_ADMIN_INJECT,
  FirebaseAdminSDK as FirebaseService,
} from '@tfarras/nestjs-firebase-admin'

@Injectable()
export class AuthService {
  constructor(@Inject(FIREBASE_ADMIN_INJECT) private readonly firebaseService: FirebaseService) {}

  getUser(userId: string) {
    return this.firebaseService.auth().getUser(userId)
  }

  async getCustomToken(uid: string) {
    const data = await this.firebaseService.auth().createCustomToken(uid)

    return { data }
  }
}
