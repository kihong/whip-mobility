import { Field, Float, ID, ObjectType } from '@nestjs/graphql'

import { VehicleObject } from '../vehicle/vehicle.object'

@ObjectType()
export class UserObject {
  @Field(_ => ID, { name: 'id' })
  uid: string

  @Field(_ => Float)
  amount: number

  @Field(_ => [VehicleObject])
  vehicles: VehicleObject[]

  @Field({ nullable: true })
  email?: string

  @Field({ nullable: true })
  displayName?: string

  @Field({ nullable: true })
  isEmailVerified?: boolean

  @Field({ nullable: true })
  phoneNumber?: string

  @Field({ name: 'picture', nullable: true })
  avatar?: string
}
