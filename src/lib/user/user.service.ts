import { Inject, Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import {
  FIREBASE_ADMIN_INJECT,
  FirebaseAdminSDK as FirebaseService,
} from '@tfarras/nestjs-firebase-admin'
import { Model } from 'mongoose'

import { VehicleDoc } from '../vehicle/vehicle.interface'

@Injectable()
export class UserService {
  constructor(
    @Inject(FIREBASE_ADMIN_INJECT) private readonly firebaseService: FirebaseService,
    @InjectModel('Vehicle') private VehicleModel: Model<VehicleDoc>
  ) {}

  async getVehicles(userId: string): Promise<VehicleDoc[]> {
    return await this.VehicleModel.find({ owner: userId })
  }

  async getDisplayName(uid: string) {
    const user = await this.firebaseService.auth().getUser(uid)
    return user?.displayName
  }
}
