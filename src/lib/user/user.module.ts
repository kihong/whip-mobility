import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { VehicleSchema } from '../vehicle/vehicle.schema'
import { UserResolver } from './user.resolver'
import { UserService } from './user.service'

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Vehicle', schema: VehicleSchema }])],
  providers: [UserResolver, UserService],
})
export class UserModule {}
