import { UseGuards } from '@nestjs/common'
import { Parent, Query, ResolveField, Resolver } from '@nestjs/graphql'
import { FirebaseUser } from '@tfarras/nestjs-firebase-auth'
import { AppGuard } from '~/app.guard'
import { AppUser } from '~/shared/decorators'

import { VehicleObject } from '../vehicle/vehicle.object'
import { UserObject } from './user.object'
import { UserService } from './user.service'

@Resolver(_of => UserObject)
@UseGuards(AppGuard)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query(_returns => UserObject, { nullable: true })
  async me(@AppUser() user: FirebaseUser) {
    return user
  }

  @ResolveField('displayName', _returns => String)
  displayName(@Parent() { uid }: FirebaseUser) {
    return this.userService.getDisplayName(uid)
  }

  @ResolveField('vehicles', _returns => [VehicleObject])
  vehicles(@Parent() { uid }: FirebaseUser) {
    return this.userService.getVehicles(uid)
  }
}
