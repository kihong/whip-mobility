export enum ServiceType {
  INTERIM = 'INTERIM',
  FULL = 'FULL',
  MAJOR = 'MAJOR',
}
