import { Schema } from 'mongoose'

import { ServiceType } from './service.interface'

export const ServiceSchema = new Schema({
  type: {
    type: String,
    enum: Object.keys(ServiceType),
    required: true,
    default: ServiceType.INTERIM,
  },
  dealer: {
    type: Schema.Types.ObjectId,
    ref: 'Dealer',
  },
  vehicle: {
    type: Schema.Types.ObjectId,
    ref: 'Vehicle',
  },
  technicians: [
    {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
})
